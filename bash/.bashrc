#
# ~/.bashrc
#

# Use gpg-agent-ssh.socket
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
  export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi
export GPG_TTY=$(tty)
gpg-connect-agent updatestartuptty /bye >/dev/null

export SUDO_ASKPASS=/usr/bin/lxqt-openssh-askpass

export TERMINAL=termite

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Display branch when in a git directory
# GIT='$(__git_ps1 " (%s)")'

# Usual-basic prompt
# PROMPT="[\u@\h \W${GIT}]\$ "

# Colours sequences (tput)
GREEN="\[$(tput setaf 2)\]"
BLUE="\[$(tput setaf 4)\]"
YELLOW="\[$(tput setaf 3)\]"
MAG="\[$(tput setaf 5)\]"
CYAN="\[$(tput setaf 6)\]"
RESET="\[$(tput sgr0)\]"

# Display branch name when in a git directory
GIT='$(__git_ps1 "(%s)")'

# Usual-basic prompt
PROMPT="${GREEN}\u${BLUE}@\h ${MAG}\W/ ${YELLOW}${GIT}${GREEN}$ "

export JAVA_HOME="/usr/lib/jvm/java-11-openjdk"
export \
    PATH="$PATH:/opt/bin:$HOME/.gem/ruby/2.6.0/bin:$HOME/.local/bin:$HOME/.cargo/bin:/usr/share/perl6/vendor/bin:$JAVA_HOME/bin:$HOME/opt/bin"
export GOPATH="$GOPATH:/home/errpell/.go"
export CDPATH=".:/home/errpell/Code"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/local/lib"
export I3SOCK=$(i3 --get-socketpath)
export I3_SOCKET_PATH=$(i3 --get-socketpath)
export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig"

# Window title
#TITLE='\e\]2; \W/ \a'

# ${whatever} colour input
export PS1="${PROMPT}${RESET}"

# Retablish output colour
trap 'tput sgr0' DEBUG

# webasm
source ~/AUR_BUILD/emsdk/emsdk_env.sh

#git
source ~/.git-prompt.sh

#color man pages
less_vars () {
    LESS=-R \
    LESS_TERMCAP_mb=$'\e[01;31m' \
	LESS_TERMCAP_md=$'\e[01;31m' \
	LESS_TERMCAP_me=$'\e[0m' \
	LESS_TERMCAP_se=$'\e[0m' \
	LESS_TERMCAP_so=$'\e[01;44;33m' \
	LESS_TERMCAP_ue=$'\e[0m' \
	LESS_TERMCAP_us=$'\e[01;32m'
}

man () {
    less_vars
    command man "$@"
}

less () {
    less_vars
    command less "$@"
}

export EDITOR=vim

export HISTSIZE=100000
export HISTFILESIZE=1000000
export HISTCONTROL=ignoreboth:erasedups
export HISTTIMEFORMAT='%s'
export PROMPT_COMMAND='history -w'

shopt -s histappend
shopt -s autocd
shopt -s checkwinsize

set -o vi

# Import colorscheme from 'wal'
(cat $HOME/.cache/wal/sequences & clear)

#Alias file
if [ -f ~/.bash_aliases ]; then
	. ~/.bash_aliases
fi

# Custom session file
if [ -f ~/.config/nhatz/session ]; then
    . ~/.config/nhatz/session
fi

export THEME=~/.config/bash/themes/agnoster/agnoster.bash
if [[ -f $THEME ]]; then
    export DEFAULT_USER=`whoami`
    . $THEME
fi

if [[ -f /usr/share/doc/pkgfile/command-not-found.bash ]]; then
    . /usr/share/doc/pkgfile/command-not-found.bash
fi

export MANPAGER="/bin/sh -c \"col -b | vim -c 'set ft=man ts=8 nomod nolist nonu noma' -\""

export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8

export JAVA_OPTIONS="$JAVA_OPTIONS -DsocksProxyHost=localhost
-DsocksProxyPort=9050"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export SSD_HOME="/run/media/errpell/T7/errpell/Home/"
