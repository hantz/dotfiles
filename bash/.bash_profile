#
# ~/.bash_profile
#


[[ -f ~/.bashrc ]] && . ~/.bashrc

nhatz_session

#locally created/compiled scripts, games and utilities
export PATH="$PATH:/opt/bin:$HOME/.gem/ruby/2.4.0/bin:$HOME/.local/bin:$HOME/.cargo/bin"
export I3SOCK="/run/user/1000/i3/ipc.socket"
export I3_SOCKET_PATH=$(i3 --get-socketpath)

export QT_QPA_PLATFORMTHEME=qt5ct

if [ -z "$DISPLAY" ] && [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 1 ]; then
	exec startx
fi
