vimruntimepath () {
  vim --cmd 'echo $VIMRUNTIME|q'
}

alias sudo='sudo '
alias docker='sudo docker'
#alias ls='ls --color=auto' # color ls output
alias ls='exa'
alias diff='diff --color=auto' # color diff output
alias grep='grep --color=auto' # color grep output
alias feh='feh --draw-filename --no-fehbg --keep-zoom-vp --scale-down' # don't overwrite existing script
alias xclip='xclip -selection c' # Pastable copy to clipboard
#alias wifi_home='wifi_setup -e BELL214 -i wlp1s0 -p 52EFF792'
alias vimruntime='vimruntimepath'
alias vim='vim --servername vim@waemis'
# alias neofetch='neofetch --ascii_distro arch_old --disable gpu  -s $HOME/Pictures/Screenshots/neofetch_"$(date +%F)"'
alias gdx_setup='java -jar /opt/nhatz/gdx-setup.jar'
alias tpack='java -jar /opt/texturepacker.jar'
alias plp='java -jar /opt/plptool5/PLPToolStatic.jar -W -D /opt/plptool/extensions/ $@'
alias absolutely_bloatprietary='python /opt/absolutely-proprietary/main.py'
alias tree='tree -I .git'
alias gopen='gio open'
alias sosc='sendosc'
alias cat='bat'
alias clang-format-diff='python /usr/share/clang/clang-format-diff.py'
