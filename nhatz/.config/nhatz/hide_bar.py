#!/usr/bin/env python
import time
import subprocess
 
DELAY = 0.5
SCREEN_HEIGHT = 1080
SCREEN_WIDTH = 1920
SHOW_CMD = 'xdo show -a polybar-music_eDP1'
HIDE_CMD = 'xdo hide -a polybar-music_eDP1'
 
def get_position():
    output = subprocess.check_output('xdotool getmouselocation --shell', shell=True).decode('utf-8').split('\n')[0:2]
    return int(output[0].split('=')[1]), int(output[1].split('=')[1])

def focused():
    active = int(subprocess.check_output("xdotool getactivewindow").decode('utf-8'))
    
    wid = int(subprocess.check_output("xdotool search \"polybar-music_eDP1\"").decode('utf-8'))
    
    return wid == active
 
hidden = True
subprocess.call(HIDE_CMD, shell=True)
while True:
    x, y = get_position()
    on_spot = (x < (1 * SCREEN_WIDTH / 100)) and  (y > SCREEN_HEIGHT - (1 *
        SCREEN_HEIGHT / 100))
    if on_spot and hidden:
        subprocess.call(SHOW_CMD, shell=True)
        hidden = False

    in_rect = x < 600 and (y > SCREEN_HEIGHT - 20)
    if (not in_rect) and (not on_spot) and (not hidden):
        subprocess.call(HIDE_CMD, shell=True)
        hidden = True
    time.sleep(DELAY)
