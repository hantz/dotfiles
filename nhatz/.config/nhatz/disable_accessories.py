#!/usr/bin/env python
import os
import subprocess

TO_DISABLE = [
        "Synaptics TM3072-003",
        "TPPS/2 IBM TrackPoint",
        "AT Translated Set 2 keyboard",
        "ThinkPad Extra Buttons"
    ]


def disable_all():
    input_info_list = os.popen("xinput list").read().split('\n')

    for input_info_item in input_info_list:
        if any(input_element.lower() in input_info_item.lower()
                for input_element in TO_DISABLE):

            if disable(extract_id(input_info_item)):
                log = "Disabled"
            else:
                log = "Couldn't disable"

            input_name = input_info_item.split("\t")[0]
            log = log + ": {0}".format(input_name)

            subprocess.run(["notify-send", "--app-name", "Nhatz Input", log])
            print(log)


def extract_id(input_info: str) -> int:
    for element in input_info.split('\t'):
        if "id=" in element:
            return int(element.split('id=')[1])

    return -1


def disable(input_id: int) -> bool:
    if input_id == -1:
        return False

    subprocess.run(["xinput", "float", str(input_id)])
    return True


def main():
    disable_all()


if __name__ == "__main__":
    main()
